package com.finki.soa.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.finki.soa.model.Person;

public interface PersonRepository extends CrudRepository<Person, Long>{
	
	List<Person> findByFirstNameAndSecondName(String firstName, String secondName);

	Person findByUsername(String username);
	
}
