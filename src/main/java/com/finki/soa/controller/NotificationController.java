package com.finki.soa.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finki.soa.model.Book;
import com.finki.soa.model.Notification;
import com.finki.soa.model.Person;
import com.finki.soa.service.BookService;
import com.finki.soa.service.NotificationService;
import com.finki.soa.service.PersonService;

@RestController
public class NotificationController {

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private BookService bookService;

	@Autowired
	private PersonService personService;

	@GetMapping("/notification")
	public Iterable<Notification> getNotifications() {
		return notificationService.findAll();
	}
	
	@GetMapping("/notification/{bookName}/losers")
	public Iterable<Notification> getLosers(@PathVariable String bookName) {
		return notificationService.findLosers(bookService.findBookByName(bookName));
	}
	
	@GetMapping("/notification/{bookName}/winner")
	public Notification getWinner(@PathVariable String bookName) {
		return notificationService.findWinner(bookService.findBookByName(bookName));
	}

	@PostMapping("/notification/notifyLosers")
	public Iterable<Notification> notifyUsersWhoLost(NotificationModel notificationModel) {
		List<String> usernames = notificationModel.getUsernames();
		Book book = bookService.findBookByName(notificationModel.getBookName());
		List<Person> users = usernames.stream().map(username -> personService.findByUsername(username))
				.collect(Collectors.toList());
		return notificationService.notifyUsersWhoLost(book, users);
	}

	@PostMapping("/notification/notifyWinner")
	public void notifyWinner(NotificationModel notificationModel) {
		String username = notificationModel.getUsernames().get(0);
		Book book = bookService.findBookByName(notificationModel.getBookName());
		Person user = personService.findByUsername(username);
		notificationService.notifyWinner(book, user);
	}

	private static class NotificationModel {
		private List<String> usernames;
		private String bookName;

		public List<String> getUsernames() {
			return usernames;
		}

		public void setUsernames(List<String> usernames) {
			this.usernames = usernames;
		}

		public String getBookName() {
			return bookName;
		}

		public void setBookName(String bookName) {
			this.bookName = bookName;
		}

	}
}